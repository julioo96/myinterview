package com.example.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
public class Employees {

    @Id
    protected int emp_no;

    protected Date birth_date;

    protected String first_name;

    protected String last_name;

    protected String gender;

    protected Date hire_date;
}
