package com.example.models;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeSalaryDTO {

    public double average;
    public double maximum;
    public double minimum;
}
