package com.example.models;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class EmployeeDistinctsDTO {

    public int emp_no;
    public Date birth_date;
    public Date hire_date;
    public String gender;
}
