package com.example.repository;


import com.example.entity.Employees;
import com.example.models.EmployeeDistinctsDTO;
import com.example.models.EmployeeSalaryDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeesRepository extends CrudRepository<Employees, Integer> {

    long countByGender(String gender);

    @Query(value = "select AVG(s.salary) as average, MAX(s.salary) as maximum, MIN(s.salary) as minimum from employees e \n" +
            "\n" +
            "inner join salaries s on s.emp_no = e.emp_no\n" +
            "\n" +  
            "where gender = ?1", nativeQuery = true)
    EmployeeSalaryDTO getAverageMaxAndMinSalaryByGender(String gender);

    @Query(value = "select COUNT(emp_no), birth_date, hire_date, gender from employees e \n" +
            "\n" +
            "group by gender, hire_date , birth_date \n" +
            "\n" +
            "order by gender", nativeQuery = true)
    EmployeeDistinctsDTO getQuantityDistinctByGenderAndBirthDateAndHireDate();
}
