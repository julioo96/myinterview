package com.example.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.example.entity.Employees;
import com.example.repository.EmployeesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class ApiService {

    @Value("${application.bucket.name}")
    private String bucketName;

    @Autowired
    private AmazonS3 s3Client;

    @Autowired
    private EmployeesRepository employeesRepository;


    public void uploadFile() throws IOException {
        String fileName = "employeesQuantity";
        long quantityMaleEmployees = employeesRepository.countByGender("M");
        long quantityFemaleEmployees = employeesRepository.countByGender("F");
        List<String> lines = Arrays.asList(String.format("Quantitade de funcionarios masculinos é: %s", quantityMaleEmployees),
                String.format("Quantitade de funcionarios femininos é: %s", quantityFemaleEmployees));
        Path filePath = Paths.get(fileName +".txt");
        Files.write(filePath, lines, StandardCharsets.UTF_8);
        File file = filePath.toFile();
        PutObjectResult putObjectResult = s3Client.putObject(new PutObjectRequest(bucketName, fileName, file));
        file.delete();
    }
}
